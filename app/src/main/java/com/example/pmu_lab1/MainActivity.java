package com.example.pmu_lab1;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

    /**
     * @param a
     * @param b
     * @return
     */
    public int min(int a, int b) {
        return a < b ? a : b;
    }

    /**
     * @param a
     * @param b
     * @return
     */
    public int max(int a, int b) {
        return a > b ? a : b;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}