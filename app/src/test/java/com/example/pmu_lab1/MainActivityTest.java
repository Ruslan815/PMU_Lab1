package com.example.pmu_lab1;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class MainActivityTest {
    MainActivity someActivity = new MainActivity();

    @Test
    public void minPositiveNumbersALessThenBTest() {
        assertEquals(someActivity.min(1, 2), 1);
    }

    @Test
    public void minPositiveNumbersBLessThenATest() {
        assertEquals(someActivity.min(2, 1), 1);
    }

    @Test
    public void minNegativeNumbersALessThenBTest() {
        assertEquals(someActivity.min(-2, -1), -2);
    }

    @Test
    public void minNegativeNumbersBLessThenATest() {
        assertEquals(someActivity.min(-1, -2), -2);
    }

    @Test
    public void maxPositiveNumbersAGreaterThenBTest() {
        assertEquals(someActivity.max(2, 1), 2);
    }

    @Test
    public void maxPositiveNumbersBGreaterThenATest() {
        assertEquals(someActivity.max(1, 2), 2);
    }

    @Test
    public void maxNegativeNumbersAGreaterThenBTest() {
        assertEquals(someActivity.max(-1, -2), -1);
    }

    @Test
    public void maxNegativeNumbersBGreaterThenATest() {
        assertEquals(someActivity.max(-2, -1), -1);
    }
}